import http from 'k6/http';
import { sleep } from 'k6';
export const options = {
  vus: 5000,
  duration: '30s',
};
export default function () {
  http.get('https://by-shop.ru/posuda-208/');
  sleep(1);
}