app_name='hato.messenger'
cp_path=$1
with_static=true

build_zip_app() {
  app_name=$1
  with_static=$2
  cp_path=$3

  echo "Creating app '$app_name' with_static='$with_static'"

  rm -rf build
  rm "$app_name.tar.gz"
  mkdir build

  mkdir build/docs
  mkdir build/static
  mkdir build/static/css

  cp -R app build/
  cp -R fixtures build/
  cp -R localization build/
  cp -R media build/
  cp -R settings build/
  cp -R docs/api build/docs/

  if [ "$with_static" = true ]; then
    echo "copy static files:"
    cp -R static/*.* build/static/
    cp -R static/font build/static/
    cp -R static/img build/static/
    cp -R static/bundle build/static/
    cp -R static/glyphter-font build/static/
    cp -R static/css/*css build/static/css/
  fi

  cp requirements.txt build/
  cp Dockerfile build/
  cp entrypoint.sh build/
  cp requirements.* build/

  rm -f build/settings/env.local.json

  cd build
  tar -zcvf "../$app_name.tar.gz" ./*
  cd ..
  rm -rf build

  echo "DONE"

  if [ ! -z "$cp_path" ]; then
    rm -f "$cp_path/$app_name.tar.gz"
    cp "$app_name.tar.gz" "$cp_path"
    echo "cp script to $cp_path"
  fi
}

build_zip_app "$app_name" "$with_static" "$cp_path"
