#!/usr/bin/env sh

set -e

wait_until_db() {

    until [ "$(nmap -p ${DB_PORT} ${DB_HOST} | grep open)" ];
    do
        sleep 1
        echo "Waiting for DB at ${DB_HOST}:${DB_PORT}..."
    done

}

if [ -n "${DEBUG}" ];
then
  echo '--- CONTAINER DEBUG INFORMATION ---'
  python3 --version
  echo '--- packages:'
  pip freeze
  echo '--- files:'
  ls .
  echo '-----------------------------------'
fi

python3 -OOm app "$@"
