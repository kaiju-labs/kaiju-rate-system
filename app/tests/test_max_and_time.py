import aiohttp

import asyncio
from kaiju_tools.encoding.json import dumps, loads
import time
from random_username.generate import generate_username
import random

list_username = generate_username(15000)
list_product = generate_username(15000)
list_reaction = ['like', 'dislike', 'maybe']
print(list_product)
print(list_username)


async def make_mark(session):
    result = await session.request("POST", "http://0.0.0.0:8700/public/rpc", data=dumps({
        "method": "RatesAndReviewService.rate.mark",
        "params": {
            "correlation_id": list_product[random.randint(0, len(list_product) - 1)],
            "user_id": list_username[random.randint(0, len(list_username) - 1)],
            "type_mark": list_reaction[random.randint(0, len(list_reaction) - 1)]
        }}
    ))
    return loads(await result.text())


async def make_list(session, product):
    result = await session.request("POST", "http://0.0.0.0:9090/public/rpc", data=dumps({
        "method": "RatesService.list_rates",
        "params": {
            "correlation_id": product,
            "types_mark": list_reaction
        }}
    ))
    return loads(await result.text())


async def make_check(session):
    result = await session.request("POST", "http://0.0.0.0:9090/public/rpc", data=dumps({
        "method": "RatesService.check",
        "params": {
            "correlation_id": list_product[random.randint(0, len(list_product) - 1)],
            "user_id": list_username[random.randint(0, len(list_username) - 1)],
        }}
    ))
    return loads(await result.text())


async def main():
    async with aiohttp.ClientSession() as session:
        task_list = []
        result_list = []
        start_time = time.monotonic()
        number_requests = 100000
        for i in range(number_requests):
            task = make_mark(session)
            task_list.append(asyncio.create_task(asyncio.wait_for(task, timeout=1000)))
        count = 0
        try:
            for i in task_list:
                result_list.append(await i)
            stop_time = time.monotonic()
            print("time avr one mark")
            print((stop_time - start_time) / number_requests)
        except:
            print(len(result_list))
        #
        # task_list = []
        # result_list = []
        # start_time = time.monotonic()
        # for i in list_product:
        #     task = make_list(session, i)
        #     task_list.append(asyncio.create_task(asyncio.wait_for(task, timeout=1000)))
        # try:
        #     for i in task_list:
        #         result_list.append(await i)
        #     stop_time = time.monotonic()
        #     print("time avr one list")
        #     print((stop_time - start_time) / len(list_product))
        #     print(result_list[0])
        #
        # except:
        #     print(len(result_list))
        # for i in result_list:
        #     if i["result"]:
        #         print(i["result"])

        task_list = []
        result_list = []
        start_time = time.monotonic()
        for i in range(number_requests):
            task = make_check(session)
            task_list.append(asyncio.create_task(asyncio.wait_for(task, timeout=1000)))
        try:
            for i in task_list:
                result_list.append(await i)
            stop_time = time.monotonic()
            print("time avr one check")
            print((stop_time - start_time) / number_requests)
            print(result_list[0])
        except:
            print(len(result_list))


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
