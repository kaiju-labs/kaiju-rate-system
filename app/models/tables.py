"""
Write your table definitions here.
"""

from datetime import datetime

import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as sa_pg

__all__ = (
    'metadata',
    'counter_rate',
    'storage_rate'
)

metadata = sa.MetaData()  #: user tables metadata

counter_rate = sa.Table('counter_rate', metadata,
                        sa.Column('id', sa_pg.BIGINT, autoincrement=True, primary_key=True),
                        sa.Column('correlation_id', sa_pg.TEXT, primary_key=True),
                        sa.Column('type', sa_pg.TEXT, primary_key=True),
                        sa.Column('value', sa_pg.BIGINT, default=0, server_default=sa.text("0")),
                        )

storage_rate = sa.Table('storage_rate', metadata,
                        sa.Column('correlation_id', sa_pg.TEXT, primary_key=True),
                        sa.Column('user_id', sa_pg.TEXT, primary_key=True),
                        sa.Column('status', sa_pg.JSONB, default={},
                                  server_default=sa.text("'{}'::jsonb"))
                        )

