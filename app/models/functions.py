"""
Write database user functions here.

Write your own functions here and add it to `__all__`.

.. code-block:: python

    class MyFunction(UserFunction):

        package = 'utils'
        name = 'my_func'
        type = sa.types.Integer

        body = \"""
        FUNCTION my_func(v integer) RETURNS integer AS $$
        BEGIN
        RETURN v * v;
        END; $$
        LANGUAGE PLPGSQL
        \"""

        def __init__(self, v: int):
            super().__init__(v)


You will be able to use them within SQLAlchemy like this:

.. code-block:: python

    import sqlalchemy as sa

    sql = sa.select([sa.func.utils.my_func(42)])
    await app.db.fetchval(sql)

or:

    .. code-block:: python

        import sqlalchemy as sa

        sql = MyFunction(42).select()
        await app.db.fetchval(sql)

"""

import sqlalchemy as sa

from kaiju_db.services import UserFunction

__all__ = []
 