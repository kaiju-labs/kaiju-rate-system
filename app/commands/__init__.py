"""
Write your CLI commands in this package and import them here.
"""

from .generate_data import GenData
from .generate_data_with_import import ImportData
from kaiju_tools.CLI import commands
commands.register_class(GenData)
commands.register_class(ImportData)