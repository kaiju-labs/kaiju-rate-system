from kaiju_tools.CLI import AbstractCommand
from kaiju_tools.encoding.json import dumps
from random_username.generate import generate_username
import random


class GenData(AbstractCommand):
    run_app = True
    service_name = 'gen-data'

    async def command(self, user_amount=1000000, correlation_amount=10000, like_per_person=2000) -> None:
        """
        """
        list_correlation = generate_username(correlation_amount)
        with open("correlation", 'w') as f:
            f.write(dumps(list_correlation))
        with open("username", 'w') as f:
            f.write("START \n")
        list_reaction = ['like', 'dislike', 'maybe']
        while user_amount > 0:
            list_username = generate_username(10000)

            for user in list_username:
                for i in range(like_per_person):
                    await self.app.services.RatesService.mark(
                        list_correlation[random.randint(0, len(list_correlation) - 1)],
                        user,
                        list_reaction[random.randint(0, len(list_reaction) - 1)])
            user_amount -= 10000
            with open("username", 'a') as f:
                f.write(dumps(list_username))
            self.logger.info("more %s user success left %s", like_per_person, user_amount)
