from kaiju_tools.services import ContextableService
from ..models import tables
from sqlalchemy import and_


class PStorageService(ContextableService):
    service_name = 'PStorage'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def init(self):
        pass

    async def mark(self, correlation_id, user_id, type_mark):
        data_status = {str(type_mark): True}
        async with self.app.services.db.acquire() as connection:
            async with connection.transaction():
                result = await connection.fetch('''INSERT INTO
                                                      storage_rate(correlation_id,user_id,status)
                                                      VALUES ($1,$2,$3)
                                                      ON CONFLICT (correlation_id, user_id) DO UPDATE
                                                      SET status = storage_rate.status || excluded.status || jsonb_build_object('is_updated',  storage_rate.status != (storage_rate.status || excluded.status))
                                                      RETURNING correlation_id, user_id, storage_rate.status
                                              ''', correlation_id, user_id, data_status)

                if result:
                    record = dict(result[0])
                    if type_mark in record['status']:
                        if not record['status'].get('is_updated', True):
                            return False
                return True

    async def un_mark(self, correlation_id, user_id, type_mark):
        data_status = {str(type_mark): False}
        async with self.app.services.db.acquire() as connection:
            async with connection.transaction():
                result = await connection.fetch('''INSERT INTO
                                                storage_rate(correlation_id,user_id,status)
                                                VALUES ($1,$2,$3)
                                                ON CONFLICT (correlation_id, user_id) DO UPDATE
                                                SET status = storage_rate.status || excluded.status || jsonb_build_object('is_updated', storage_rate.status != (storage_rate.status || excluded.status))
                                                RETURNING correlation_id, user_id, storage_rate.status
                                    ''', correlation_id, user_id, data_status)
                if result:
                    record = dict(result[0])
                    if type_mark in record['status']:
                        if not record['status'].get('is_updated', True):
                            return False
                return True

    async def check(self, correlation_id, user_id):
        select_statement = tables.storage_rate.select().where(and_(
            tables.storage_rate.c.correlation_id == correlation_id,
            tables.storage_rate.c.user_id == user_id,
        ))
        async with self.app.services.db.acquire() as connection:
            result = await connection.fetch(select_statement)
            if result:
                record = dict(result[0])
                return record['status']
        return {}

    async def import_rate(self, list_rate):
        dict_request = {"mark": {}, "unmark": {}}
        response_list = {"mark": [], "unmark": []}
        for i in range(len(list_rate)):
            if list_rate[i]['type_mark'] not in dict_request[list_rate[i].get("method", "mark")]:
                dict_request[list_rate[i].get("method", "mark")][list_rate[i]['type_mark']] = {}
            if list_rate[i].get("method", "mark") == "mark":
                status = True
            else:
                status = False
            dict_request[list_rate[i].get("method", "mark")][list_rate[i]['type_mark']][
                (list_rate[i]['correlation_id'], list_rate[i]['user_id'], list_rate[i]['type_mark'])] = {
                'correlation_id': list_rate[i]['correlation_id'],
                'user_id': list_rate[i]['user_id'],
                'status': {str(list_rate[i]['type_mark']): status}
            }
        async with self.app.services.db.begin() as connection:
            for mark_or_unmark, dict_req in dict_request.items():
                for type_mark, list_request in dict_req.items():
                    res = await connection.fetch('''
                                                       INSERT
                                                       INTO
                                                       storage_rate(correlation_id,user_id,status)
                                                       (SELECT
                                                           r ->> 'correlation_id',
                                                           r ->> 'user_id',
                                                           (r -> 'status')::jsonb
                                                       FROM
                                                           unnest($1::jsonb[]) as r
                                                       )
                                                ON CONFLICT (correlation_id, user_id) DO UPDATE
                                                SET status = storage_rate.status || excluded.status 
                                                || jsonb_build_object('is_updated', storage_rate.status != (storage_rate.status || excluded.status))
                                                RETURNING correlation_id, user_id, storage_rate.status
                                                ''', list_request.values())
                    for row in res:
                        record = dict(row)
                        if record['status'].get('is_updated', True):
                            response_list[mark_or_unmark].append({"correlation_id": record['correlation_id'], "type": type_mark})

        return response_list
