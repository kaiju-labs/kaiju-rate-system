"""
Import all your custom services here.
"""
from .rates import *
from .storage import *
from .redis_consumer import  *