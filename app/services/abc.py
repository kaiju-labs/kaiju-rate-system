from abc import ABC
from typing import Dict
from kaiju_tools.services import ContextableService


class StorageService(ABC, ContextableService):
    async def mark(self, correlation_id, user_id, type_mark) -> bool:
        pass

    async def un_mark(self, correlation_id, user_id, type_mark) -> bool:
        pass

    async def check(self, correlation_id, user_id) -> Dict[str, bool]:
        pass
