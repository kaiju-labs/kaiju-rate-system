import hashlib
from uuid import UUID
from functools import wraps


def generate_hash(*args):
    key = "#".join(args)
    return hashlib.md5(key.encode('utf-8')).hexdigest()


def to_uuid(id):
    if isinstance(id, UUID):
        return id

    hash_object = hashlib.md5(id.encode())
    md5_hash = hash_object.hexdigest()
    return UUID(md5_hash)


def group_elements(iterable, n):
    """Yield n-sized chunks from iterable"""
    return [iterable[i:i + n] for i in range(0, len(iterable), n)]


def offset(page, per_page):
    if per_page:
        return (page - 1) * per_page

    return 0


def pagination(page, per_page, count):
    if per_page:
        pages = count // per_page + bool(count % per_page)
    else:
        pages = 1

    return {
        "page": page,
        "pages": pages,
    }


def to_string_list(value):
    if type(value) is list:
        value = [str(i) for i in value]
    else:
        value = [str(value)]

    return value


def normalize(value):
    if type(value) is list:
        _val = []
        for i in value:
            if isinstance(i, UUID):
                i = str(i)
            _val.append(i)
        value = _val

    return value


def label_case(label, id):
    return label if label else f"""[{id}]"""


def label_constrictor(result, key='data'):
    for i in result[key]:
        if not i["label"]:
            i["label"] = f"""[{i["id"]}]"""

    return result


def label_to_dict(labels):
    _l = {}
    if type(labels) is list:
        for _label in labels:
            _l[_label["locale"]] = _label["value"]
    else:
        return labels
    return _l


def add_subject(function):
    @wraps(function)
    async def wrapper(self, *args, **kwargs):
        if "subject" not in kwargs:
            kwargs["subject"] = self.subject

        return await function(self, *args, **kwargs)

    return wrapper


def add_entity(function):
    @wraps(function)
    async def wrapper(self, *args, **kwargs):
        if "entity" not in kwargs:
            kwargs["entity"] = self.entity

        return await function(self, *args, **kwargs)

    return wrapper


