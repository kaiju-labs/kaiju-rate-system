
import uuid

from kaiju_tools.fixtures import ContextableService
from kaiju_tools.rpc.abc import AbstractRPCCompatible
from sqlalchemy import and_
import app.models.tables as tables




class RatesService(ContextableService, AbstractRPCCompatible):
    service_name = "RatesService"

    @property
    def routes(self):
        return {
            "import": self.import_rates,
            "list_rates": self.list_rate,
            "check": self.check,
        }

    @property
    def permissions(self):
        return {
            self.DEFAULT_PERMISSION: self.PermissionKeys.GLOBAL_GUEST_PERMISSION
        }

    def __init__(self, storage_rate, sync_interval=10000, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sync_interval = sync_interval
        self.storage_rate = None
        self.storage_rate_name = storage_rate

    async def init(self):
        """
        CREATE EXTENSION  IF NOT EXISTS  pg_ivm;

        """

        self.storage_rate = self.discover_service(self.storage_rate_name, required=True)
        await self.app.services.db.fetch("""CREATE EXTENSION  IF NOT EXISTS  pg_ivm""")
        try:
            await self.app.services.db.fetch("""
                                                SELECT create_immv('counter_2', 'SELECT correlation_id, type, sum(value) 
                                                FROM counter_rate GROUP BY correlation_id, type')
                                                """
                                             )
        except:
            self.logger.info("immv created yet")

    async def list_rate(self, correlation_id, types_mark = None ):
        if isinstance(correlation_id, uuid.UUID):
            correlation_id = str(correlation_id)
        if isinstance(correlation_id, str):
            correlation_id = [correlation_id]
        if isinstance(types_mark, str):
            types_mark = [types_mark]
        if types_mark is None:
            statement = """
                 SELECT * from counter_2 WHERE correlation_id = any($1::text[])
             """
        else:
            statement = """
                SELECT * from counter_2 WHERE correlation_id = any($1::text[]) AND type = any($2::text[])
            """
        result = await self.app.services.db.fetch(statement, tuple(correlation_id), tuple(types_mark))
        response = list(result)
        return response

    async def import_rates(self, list_rates):
        """
        list_rates = [{
                        "correlation_id":str,
                        "user_id":str,
                        "type_mark":str,
                        "mark_or_unmark":bool
                        },
                        ....]
        """

        while list_rates:
            marked_list = await self.storage_rate.import_rate(list_rates[:30000])
            aggregate_counter = {}
            for direction, result in marked_list.items():
                if direction == "mark":
                    dir_value = 1
                else:
                    dir_value = -1
                for event in result:
                    if (event['correlation_id'], event['type']) not in aggregate_counter:
                        aggregate_counter[(event['correlation_id'], event['type'])] = {
                            "correlation_id": event['correlation_id'],
                            "type": event['type'],
                            "value": 0}

                    aggregate_counter[(event['correlation_id'], event['type'])]["value"] += dir_value
            request_list = list(aggregate_counter.values())
            async with self.app.services.db.begin() as connection:
                await connection.fetch('''
                                        INSERT INTO
                                        counter_rate(correlation_id,type,value)
                                        (SELECT
                                                   r ->> 'correlation_id',
                                                   r ->> 'type',
                                                   (r ->> 'value')::BIGINT as value
                                               FROM
                                                   unnest($1::jsonb[]) as r
                                               )
                     
                                        ''', request_list)
            list_rates = list_rates[30000:]

    async def check(self, correlation_id, user_id):
        return await self.storage_rate.check(correlation_id, user_id)
