from kaiju_redis import RedisConsumer, RedisListener
from kaiju_tools.functions import retry


class RateConsumer(RedisConsumer):

    async def _process_batch(self, batch):

        acks = []
        for topic, rows in batch.items():
            list_parameters = []
            for row in rows:
                row_id, data = row
                headers, body = data[b'h'], data[b'b']
                if body:
                    body = self._serializer.loads(body) if body else None
                    self.logger.info(body)
                    list_parameters.append(body)
                acks.append(row_id)
            if list_parameters:
                await self.app.services.RatesService.import_rates(list_parameters)

        if acks:
            await retry(
                self._transport.execute_command, args=('XACK', self.topic, self.group_id, *acks),
                retries=10, retry_timeout=1.0, logger=self.logger)


class RateListener(RedisListener):
    consumer_class = RateConsumer
